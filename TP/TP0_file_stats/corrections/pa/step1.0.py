

path = 'data/file1.txt'

nb_numbers = 0
mysum = 0.

with open(path) as f:
    for line in f:
        number = float(line)
        nb_numbers += 1
        mysum += number

average = mysum / nb_numbers

print(('mysum =      {}\n'
       'nb_numbers = {}\n'
       'average =    {}').format(mysum, nb_numbers, average))
