#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def load_data():
    """ Loading the data in a single dictionnary """
    path = '../data/synop-2016.csv'
    station = ""
    with open(path) as f:
        f.readline()
        for line in f:
            ch = line.split(",")
            date = ch[1]
            wind = float(ch[2])
            temperature = float(ch[3]) - 273.15
            humidity = float(ch[4])
            rainfall = ch[5]
            if rainfall.strip() == "":
                rainfall = 0
            else:
                rainfall = float(rainfall)
            c = ch[6]
            station = c.strip()
            dic_station[date.split(":")[0]] = [wind, temperature, humidity, rainfall]
    return station


def get_max_temperature():
    """ Returns the date of max temperature and its value for one station """
    max_temperature = 0
    date_max_temperature = []
    for val in dic_station.keys():
        measurement = dic_station.get(val)
        if measurement[1] > max_temperature:
            max_temperature = measurement[1]
            date_max_temperature = []
            date_max_temperature.append(val)
        else:
            if measurement[1] == max_temperature:
                date_max_temperature.append(val)
    return date_max_temperature, max_temperature


def get_average_temperature():
    """ Returns the average temperature for one station """
    average_temperature = 0.0
    for val in dic_station.keys():
        measurement = dic_station.get(val)
        average_temperature = average_temperature + measurement[1]

    return average_temperature/len(dic_station)


def get_hours_humidity(rate):
    """ Returns the number of hours with humidity under a certain rate """
    period_humidity = 0
    for val in dic_station.keys():
        measurement = dic_station.get(val)
        humidity = measurement[2]
        if int(humidity) < rate:
            period_humidity = period_humidity + 1

    return period_humidity * 3


def get_sum_rainfall():
    """ Returns the sum of precipitation for one station """
    sum_rainfall = 0.0
    for val in dic_station.keys():
        measurement = dic_station.get(val)
        rainfall = measurement[3]
        sum_rainfall = sum_rainfall + rainfall
    return sum_rainfall


def period_without_rainfall():
    """ Returns the max period without rainfall """
    period_max = 0
    period = 0
    date = ""
    date_min = 0
    date_max = 0

    for val in dic_station.keys():
        precip = dic_station.get(val)
        mesure = precip[3]
        if float(mesure) <= 0.00:
            period = period + 1
        else:
            if period >= period_max:
                period_max = period
                date_min = date
                date_max = val
            period = 0
            date = val

    return date_min, date_max, period_max / 8


dic_station = {}

station_name = load_data()

print("Station :", station_name)

date_maxT, maxT = get_max_temperature()
print("the biggest temperature  @ ", station_name, " is:",
      maxT, "reached at", date_maxT)


print("the temperature average for ", station_name, " is: {0:.2f}"
      .format(get_average_temperature()))

print("the rainfall sum @ ", station_name, " is: {0:.2f}"
      .format(get_sum_rainfall()))
d, e, f = period_without_rainfall()
print("there is no rain from ", d, "to ", e, "so ", f, " days")
rate_humidity = 60
print("With a humidity inferior of ", rate_humidity, "% there are {0:.2f}"
      .format(get_hours_humidity(rate_humidity)), " hours")
print("##################\n")

