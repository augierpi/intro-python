#!/usr/bin/env python3
#-*- coding: utf-8 -*-


def load_data(file_path):
    """ Loading the data in multiple dictionaries """
    station = ""
    wind = {}
    temperature = {}
    humidity = {}
    rainfall = {}

    with open(file_path) as f:
        f.readline()
        for line in f:
            value_fields = line.split(",")
            date = value_fields[1]
            # Format date to remove minutes and seconds
            formatted_date = date.split(":")[0]
            # Add to dicts
            wind[formatted_date] = float(value_fields[2])
            temperature[formatted_date] = float(value_fields[3]) - 273.15
            humidity[formatted_date] = float(value_fields[4])
            raw_rainfall = value_fields[5]
            if raw_rainfall.strip() == "":
                rainfall[formatted_date] = 0.
            else:
                rainfall[formatted_date] = float(raw_rainfall)
            station = value_fields[6].strip()
    return station, wind, temperature, humidity, rainfall


def get_max_temperature(temperature_dict):
    """ Returns the date of max temperature and its value for one station """
    max_temperature = 0
    date_max_temperature = []
    for current_date, temp in temperature_dict.items():
        if temp > max_temperature:
            max_temperature = temp
            date_max_temperature = [current_date]
        else:
            if temp == max_temperature:
                date_max_temperature.append(current_date)
    return date_max_temperature, max_temperature


def get_average_temperature(temperature_dict):
    """ Returns the average temperature for one station """
    sum_temperatures = 0.0
    for temp in temperature_dict.values():
        sum_temperatures += temp

    return sum_temperatures / len(temperature_dict)


def get_hours_humidity(humidity_dict, rate):
    """ Returns the number of hours with humidity under a certain rate """
    period_under_rate = 0
    for humidity in humidity_dict.values():
        if humidity < rate:
            period_under_rate = period_under_rate + 1

    return period_under_rate * 3


def get_sum_rainfall(rainfall_dict):
    """ Returns the sum of precipitation for one station """
    sum_rainfall = 0.0
    for measured_rainfall in rainfall_dict.values():
        sum_rainfall += measured_rainfall
    return sum_rainfall


def period_without_rainfall(rainfall_dict):
    """ Returns the max period without rainfall """
    period_max = 0
    period = 0
    date = ""
    date_min = 0
    date_max = 0

    for current_date, measured_rainfall in rainfall_dict.items():
        if float(measured_rainfall) <= 0.00:
            period = period + 1
        else:
            if period >= period_max:
                period_max = period
                date_min = date
                date_max = current_date
            period = 0
            date = current_date

    return date_min, date_max, period_max/8

station_name, station_wind, station_temp, station_humidity, station_rainfall = load_data('../data/synop-2016.csv')
print("Station :", station_name)

date_maxT, maxT = get_max_temperature(station_temp)
print("the biggest temperature  @ ", station_name, " is:",
      maxT, "reached at", date_maxT)


print("the temperature average for ", station_name, " is: {0:.2f}"
      .format(get_average_temperature(station_temp)))

print("the rainfall sum @ ", station_name, " is: {0:.2f}"
      .format(get_sum_rainfall(station_rainfall)))
start_date, end_date, period = period_without_rainfall(station_rainfall)
print("there is no rain from ", start_date, "to ", end_date, "so ", period, " days")
rate_humidity = 60
print("With a humidity inferior of ", rate_humidity, "% there are {0:.2f}"
      .format(get_hours_humidity(station_humidity, rate_humidity)), " hours")
print("##################\n")
