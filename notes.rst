
Table of contents
=================

day 0 : Introduction to Python
------------------------------

- Why Python ? Demos (adapted to the public)

- First steps

  - Python interpreter

  - TP: use the commands python and ipython (run files and interactively)

  - Remark python 2 / python 3

  - TP: intro IDE (spyder)

  - Notebooks: Python in a browser

- Characteristics of the Python language

  - Open-source language, interpreters and environment

  - Indentation defines the blocks

  - Dynamic language

  - Multi-paradigm language (sequential, object-oriented, functional)

  - Interpreted (but there are tools to compile Python code)

  - A philosophy: the `Zen of Python
    <https://www.python.org/dev/peps/pep-0020/>`_

  - Remark style and pep8 (+ TP setup spyder)

  - "Batteries Included": `the standard library
    <https://docs.python.org/2/tutorial/stdlib.html>`_

- Dynamic typing: types of expressions and variables

  - the function `type`

  - string (str), int, float, boolean (bool)

  - variables are just tags pointing towards objects

- Lists and constructions

  - list

  - loops (while, for)

  - iterator (range)

  - conditions: if, elif, else

  - exceptions and try, except syntax

- Mutable and unmutable objects

- Functions

  - def, return

  - lambda

  - call: a new namespace is created and the objects are "passed by reference".

- Read/write files

- Personal work: load file, look for strings, iteration, compute sums.

Day 1 : 
-------

- Imports, multi-file program

- The standard library + presentation of few very common packages

  - sys
  - os
  - math
    
- Data structures

  - set
  - dict
  - list
  - tuple
    
- Object-oriented programming

- Exceptions

- Decorators

- Personal work, depending on the participant:

  Introduction to Numpy || manip SGBD || nombreuses arborescences

- Links to go further
